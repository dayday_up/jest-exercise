// eslint-disable-next-line no-unused-vars
import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const callback = jest.fn(() => 'O_o');
  sendBombSignal(callback);
  expect(callback).toBeCalledWith('O_o');
});
